List of Steps to Reproduce Rank-LIME:\
1. Download LETOR4.0 MSLR Dataset (10k queries, 136 features per query document vector).\
2. Train PointWise(Linear Regression), PairWise(RankNet) and ListWise(LambdaMart) learning to rank models on this data set.\
3. Get ranked document-id lists for each query corresponding to each of the models. Sort results according to score to get ordered list of documents ids corresponding to blackbox models.\
4. Clone the LIME Repository from https://github.com/marcotcr/lime \
5. The original LIME work provides three types of explainers : TextExplainers, ImageExplainers and TabularExplainers(for numerical data) for explaining predictions in binary classification. \
6. In this work we modify the preturbation function of the tabular explainer(https://github.com/marcotcr/lime/blob/master/lime/lime_tabular.py) to extend LIME to explain ranking models. \
7. Evaluation metrics : Model Faithfulness(Kendall's Tau, Ranklib implementation), NDCG@10(Trec eval), Interpretability(normalized count of number of non-zero weights).

